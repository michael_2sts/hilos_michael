import java.util.concurrent.Semaphore;

public class ATMMain {
    public static void main(String[] args) {

        //añadimos el semaforo poniendo que solo se ejecuten 3 hilos a la vez

        Semaphore semaforo1 = new Semaphore(3);

        for (int i = 1; i <= 5; i++) {
            //creamos el bucle for para que hayan 5 cajeros con un nombre distinto cada uno

            Thread t = new Thread(new ATM(semaforo1, "Cajero ATM- "+ i));
            t.start();
            //empezamos el hilo
        }

    }
}