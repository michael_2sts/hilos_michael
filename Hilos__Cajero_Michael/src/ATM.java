import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadLocalRandom;

public class ATM extends Thread{
    String name ;
    Semaphore semaforo;

    public ATM(Semaphore semaforo1, String name){
        this.semaforo = semaforo1;
        this.name = name;
    }


    @Override
    public void run(){
        try {
            //con el .acquire hacemos que no empieze hasta que el semaphore lo permita

            semaforo.acquire();
            System.out.println(name + " esta libre");

            //creamos un tiempo de uso entre 3 y 10

            int tiempoRandom = ThreadLocalRandom.current().nextInt(3000,10000+1000);
            //lo dividimos entre 1000 para pasar de milisegundos a segundos y asi poder mostrarlo
            int tiempoUsoSegundos = tiempoRandom/1000;

                for (int i = 1; i <= tiempoUsoSegundos; i++) {
                    System.out.println(name+ " esta en uso (" + i+ "s)" );
                    //hacemos que descanse 1 s
                    Thread.sleep(1000);
                }
            System.out.println("\n"+name + " esta libre despues de " + tiempoUsoSegundos +"s\n");

            semaforo.release();
            //con el .release confirmamos que ha finalizado el hilo para que empieze el siguiente

        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

    }

}
