public class CajaHilo extends Thread {
    Caja caja;

    public CajaHilo(Caja caja) {
        this.caja = caja;
    }

    @Override
    public void run() {
        /*llamamos al metodo de la clase caja, en el que crearemos un numero aleatorio de cliente de 1 a 100
        y otro aleatorio de la cantidad de productos de 1 a 10 */

        int numero_cliente= (int) (Math.random() * (100 - 1 + 1) + 1);
        int cantidad_de_productos = (int) (Math.random() * (10 - 1 + 1) + 1);

        try {
            caja.procesarCompra("Cliente: " + numero_cliente, cantidad_de_productos);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

