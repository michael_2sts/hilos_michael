import java.util.concurrent.Semaphore;

public class Supermercado {

    public static void main(String[] args) {

        //añadimos el semaforo poniendo que solo se ejecuten 2 hilos a la vez
        Semaphore semaforo = new Semaphore(2);

        //creamos los 3 objetos caja con un nombre distinto cada una

        Caja caja1 = new Caja("Caja 1", semaforo);
        Caja caja2 = new Caja("Caja 2", semaforo);
        Caja caja3 = new Caja("Caja 3", semaforo);

        //creamos los 3 hilos caja que recibira objetos de arriba

        CajaHilo hilo1 = new CajaHilo(caja1);
        CajaHilo hilo2 = new CajaHilo(caja2);
        CajaHilo hilo3 = new CajaHilo(caja3);

        //inicio todos los hilos

        hilo1.start();
        hilo2.start();
        hilo3.start();

        //Compruebo que todos los hilos han acabado para poder poner el mensaje final

        try {
            hilo1.join();
            hilo2.join();
            hilo3.join();

            System.out.println("Todas las cajas estan libres");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

    }







}
