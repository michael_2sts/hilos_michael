import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadLocalRandom;

public class Caja {

    String nombre;
    Semaphore semaforo;

    public Caja(String s, Semaphore semaforo1) {
        this.nombre = s;
        this.semaforo = semaforo1;
    }

    //creamos le metodo procesarCompra
    public void procesarCompra (String cliente, int productos) {
        int tiempoEspera = 0;

        try {

            //con el .acquire hacemos que no empieze hasta que el semaphore lo permita

            semaforo.acquire();
            System.out.println("La " + nombre + " recible al " + cliente );



            for (int i = 1; i <= productos ; i++) {
                //creamos un tiempo de espera entre productos de 1 a 10

                tiempoEspera = ThreadLocalRandom.current().nextInt(0,10000+1000);

                //ejecutamos el tiempo de espera con el .sleep

                Thread.sleep(tiempoEspera);
                System.out.println(nombre +" ," + cliente + " ," + "Producto: "+ i + (" ("+tiempoEspera/1000+"s)"));
            }
            System.out.println("\nLa " + nombre + " esta libre. Total de productos procesados: " + productos+"\n");

            //con el .release confirmamos que ha finalizado el hilo para que empieze el siguiente

            semaforo.release();

        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

    }

}


